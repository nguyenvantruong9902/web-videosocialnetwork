import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './body/home/home.component';
import { ProfilemainComponent } from './body/profile/profilemain/profilemain.component';
import { ShowvideoComponent } from './body/showvideo/showvideo.component';


const routes: Routes = [
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path:'home',
    component:HomeComponent
  },
  {
    path:'profile',
    component:ProfilemainComponent
  },
  {
    path:'showvideo',
    component:ShowvideoComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
