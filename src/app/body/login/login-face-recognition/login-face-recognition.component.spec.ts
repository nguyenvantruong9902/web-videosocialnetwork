import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoginFaceRecognitionComponent } from './login-face-recognition.component';

describe('LoginFaceRecognitionComponent', () => {
  let component: LoginFaceRecognitionComponent;
  let fixture: ComponentFixture<LoginFaceRecognitionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoginFaceRecognitionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginFaceRecognitionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
