import { Component, OnInit, Input, Inject } from '@angular/core';

@Component({
  selector: 'app-statusbar',
  templateUrl: './statusbar.component.html',
  styleUrls: ['./statusbar.component.css']
})
export class StatusbarComponent implements OnInit {
  @Input() snav: any;
  constructor() { }

  ngOnInit() {
  }

}
