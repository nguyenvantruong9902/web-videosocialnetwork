import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-showvideo',
  templateUrl: './showvideo.component.html',
  styleUrls: ['./showvideo.component.css']
})
export class ShowvideoComponent implements OnInit {
  screenWidth: number;
  constructor() { }
  items = Array.from({length: 10})
  ngOnInit() {
    this.responsiveAdmin();
  }
  responsiveAdmin() {
    this.screenWidth = window.innerWidth;
    window.onresize = () => {
      this.screenWidth = window.innerWidth;
    };
  }
}
