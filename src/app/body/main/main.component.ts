import { Component, OnInit, Input, Inject } from '@angular/core';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {
  screenWidth: number;
  constructor() { }
  
  ngOnInit() {
    this.responsiveAdmin();
  }
  responsiveAdmin() {
    this.screenWidth = window.innerWidth;
    window.onresize = () => {
      this.screenWidth = window.innerWidth;
    };
  }
}
